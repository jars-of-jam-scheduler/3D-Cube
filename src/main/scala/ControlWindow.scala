import java.awt.Graphics
import javax.swing._

import Main.{canvas, world_cube_points}

/**
  * The window allowing to transform the object
  */
class ControlWindow extends JFrame {

  /**
    * Displays the window
    */
  def display(): Unit = {
    setTitle("Control of the transformations")
    setSize(400, 400)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    setVisible(true)
  }

  /**
    * Fulfills the control window and sets events handlers
    * @param graphics the Graphics object
    */
  override def paint(graphics : Graphics): Unit = {
    super.paint(graphics)

    val label_1 : JLabel = new JLabel("Translation :")
    val label_2 : JLabel = new JLabel("Scaling :")
    val label_3 : JLabel = new JLabel("Rotation :")

    val spinner_1 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.5))
    val spinner_2 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.5))
    val spinner_3 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.5))

    val spinner_4 = new JSpinner(new SpinnerNumberModel(1, -1, 2, 0.1))
    val spinner_5 = new JSpinner(new SpinnerNumberModel(1, -1, 2, 0.1))
    val spinner_6 = new JSpinner(new SpinnerNumberModel(1, -1, 2, 0.1))

    val spinner_7 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.1))
    val spinner_8 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.1))
    val spinner_9 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.1))

    val layout = new BoxLayout(this.getContentPane, BoxLayout.Y_AXIS)
    this.getContentPane.setLayout(layout)

    this.add(label_1)
    this.add(spinner_1)
    this.add(spinner_2)
    this.add(spinner_3)

    this.add(label_2)
    this.add(spinner_4)
    this.add(spinner_5)
    this.add(spinner_6)

    this.add(label_3)
    this.add(spinner_7)
    this.add(spinner_8)
    this.add(spinner_9)

    /* <!--------------------------------------------------------------------------------- BEGINNING - TRANSLATION -> */
    spinner_1.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val translation_matrix = new Matrix(Seq(
          Seq(1, 0, 0, 0),
          Seq(0, 1, 0, 0),
          Seq(0, 0, 1, 0),
          Seq(new_val, 0, 0, 1)
        ))
        world_cube_points = new Matrix(world_cube_points.content.map(p => translation_matrix.product(p)))
        canvas.repaint()
      }

      spinner_1.setValue(0.0)
    })

    spinner_2.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val translation_matrix = new Matrix(Seq(
          Seq(1, 0, 0, 0),
          Seq(0, 1, 0, 0),
          Seq(0, 0, 1, 0 ),
          Seq(0, -new_val, 0, 1)
        ))
        world_cube_points = new Matrix(world_cube_points.content.map(p => translation_matrix.product(p)))
        canvas.repaint()
      }

      spinner_2.setValue(0.0)
    })

    spinner_3.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val translation_matrix = new Matrix(Seq(
          Seq(1, 0, 0, 0),
          Seq(0, 1, 0, 0),
          Seq(0, 0, 1, 0 ),
          Seq(0, 0, new_val, 1)
        ))
        world_cube_points = new Matrix(world_cube_points.content.map(p => translation_matrix.product(p)))
        canvas.repaint()
      }

      spinner_3.setValue(0.0)
    })
    /* <!--------------------------------------------------------------------------------- END - TRANSLATION -> */



    /* <!------------------------------------------------------------------------------------- BEGINNING - SCALING -> */
    spinner_4.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val scaling_matrix = new Matrix(Seq(
          Seq(new_val, 0, 0, 0),
          Seq(0, 1, 0, 0),
          Seq(0, 0, 1, 0 ),
          Seq(0, 0, 0, 1)
        ))
        world_cube_points = new Matrix(world_cube_points.content.map(p => scaling_matrix.product(p)))
        canvas.repaint()
      }

      spinner_4.setValue(1.0)
    })

    spinner_5.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val scaling_matrix = new Matrix(Seq(
          Seq(1, 0, 0, 0),
          Seq(0, new_val, 0, 0),
          Seq(0, 0, 1, 0 ),
          Seq(0, 0, 0, 1)
        ))
        world_cube_points = new Matrix(world_cube_points.content.map(p => scaling_matrix.product(p)))
        canvas.repaint()
      }

      spinner_5.setValue(1.0)
    })

    spinner_6.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val scaling_matrix = new Matrix(Seq(
          Seq(1, 0, 0, 0),
          Seq(0, 1, 0, 0),
          Seq(0, 0, new_val, 0 ),
          Seq(0, 0, 0, 1)
        ))
        world_cube_points = new Matrix(world_cube_points.content.map(p => scaling_matrix.product(p)))
        canvas.repaint()
      }

      spinner_6.setValue(1.0)
    })
    /* <!------------------------------------------------------------------------------------- END - SCALING -> */




    /* <!------------------------------------------------------------------------------------ BEGINNING - ROTATION -> */
    spinner_7.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val rotation_matrix = new Matrix(Seq(
          Seq(1, 0, 0, 0),
          Seq(0, Math.cos(new_val), -Math.sin(new_val), 0),
          Seq(0, Math.sin(new_val), Math.cos(new_val), 0),
          Seq(0, 0, 0, 1)
        ))

        world_cube_points = new Matrix(world_cube_points.content.map(p => rotation_matrix.product(p)))
        canvas.repaint()
      }

      spinner_7.setValue(0.0)
    })

    spinner_8.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val rotation_matrix = new Matrix(Seq(
          Seq(Math.cos(new_val), 0, Math.sin(new_val), 0),
          Seq(0, 1, 0, 0),
          Seq(-Math.sin(new_val), 0, Math.cos(new_val), 0),
          Seq(0, 0, 0, 1)
        ))

        world_cube_points = new Matrix(world_cube_points.content.map(p => rotation_matrix.product(p)))
        canvas.repaint()
      }

      spinner_8.setValue(0.0)
    })

    spinner_9.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]

      if(new_val != 0) {
        val rotation_matrix = new Matrix(Seq(
          Seq(Math.cos(new_val), -Math.sin(new_val), 0, 0),
          Seq(Math.sin(new_val), Math.cos(new_val), 0, 0),
          Seq(0, 0, 1, 0),
          Seq(0, 0, 0, 1)
        ))

        world_cube_points = new Matrix(world_cube_points.content.map(p => rotation_matrix.product(p)))
        canvas.repaint()
      }

      spinner_9.setValue(0.0)
    })
    /* <!------------------------------------------------------------------------------------ END - ROTATION -> */


  }

}
