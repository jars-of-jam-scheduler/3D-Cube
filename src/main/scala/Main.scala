object Main {

  /**
    * The 3D to 2D perspective projector
    */
  val projector = new Projector()

  /**
    * The linear interpolator
    */
  val interpolator = new LinearInterpolator()

  /**
    * The noises maker
    */
  val noise_maker = new NoisesGenerator()

  /**
    * The world cube's points
    */
  var world_cube_points : Matrix = new Matrix(Seq(
    Seq(-5, 5, -5, 1),  // left top far
    Seq(-5, 5, 5, 1),  // left top close
    Seq(5, 5, -5, 1),  // right top far
    Seq(5, 5, 5, 1),  // right top close

    Seq(-5, -5, -5, 1),  // left bottom far
    Seq(-5, -5, 5, 1),  // left bottom close
    Seq(5, -5, -5, 1),  // right bottom far
    Seq(5, -5, 5, 1)  // right bottom close
  ))

  /**
    * The translation transformation matrix, world to camera
    */
  var translation_matrix_world_to_camera : Matrix = new Matrix(Seq(
    Seq(1, 0, 0, 0),
    Seq(0, 1, 0, 0),
    Seq(0, 0, -1, 0),
    Seq(1, 1, -20, 1)
  ))

  /**
    * The canvas
    */
  val canvas : Canvas = new Canvas

  def main(args : Array[String]) : Unit = {

    noise_maker.setNumberOfLayers(3)  // Configuring the noises maker

    canvas.display()
    new ControlWindow().display()

  }

}
