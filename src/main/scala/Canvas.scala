import java.awt.Graphics
import javax.swing.JFrame

import Main._

/**
  * 1 unit forward the camera.
  * Contains the drawn points.
  */
class Canvas extends JFrame {

  /**
    * The canvas' width
    */
  val CANVAS_WIDTH = 2

  /**
    * The canvas' height
    */
  val CANVAS_HEIGHT = 2

  /**
    * The image's width
    */
  val IMAGE_WIDTH = 800

  /**
    * The image's height
    */
  val IMAGE_HEIGHT = 800

  /**
    * The points to draw
    */
  var points : Seq[Seq[Double]] = Seq.empty


  /**
    * Sets the points to draw
    * @param drawn_points the points to draw
    */
  def setPointsToDraw(drawn_points : Seq[Seq[Double]]) : Unit = {
    this.points = drawn_points
  }

  /**
    * Displays the window
    */
  def display(): Unit = {
    setTitle("Perlin")
    setSize(IMAGE_WIDTH, IMAGE_HEIGHT)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    setVisible(true)
  }

  /**
    * Draws the points and displays them
    */
  def displayObject() : Unit = {
    canvas.setPointsToDraw(projector.projectPointsOnImageView(world_cube_points.content.map(point => {
      translation_matrix_world_to_camera.product(point)
    })))
  }

  /**
    * Gets the maximal and minimal values for X-coordinate, within the whole face
    * @param face the face from which you want to get the maximal and minimal values for X-coordinate
    * @return the maximal and minimal values for X-coordinate, within the whole face
    */
  def getXMaxAndXMinForFace(face : Seq[Seq[Int]]) : (Int, Int) = {
    val seq = face.sortWith((point_1 : Seq[Int], point_2 : Seq[Int]) => {
      point_1.head < point_2.head
    })
    (seq.last.head, seq.head.head)
  }

  /**
    * Gets the maximal and minimal values for Y-coordinate, within the whole face
    * @param face the face from which you want to get the maximal and minimal values for Y-coordinate
    * @return the maximal and minimal values for Y-coordinate, within the whole face
    */
  def getYMaxAndYMinForFace(face : Seq[Seq[Int]]) : (Int, Int) = {
    val seq = face.sortWith((point_1 : Seq[Int], point_2 : Seq[Int]) => {
      point_1(1) < point_2(1)
    })
    (seq.last(1), seq.head(1))
  }

  /**
    * Normalizes, rasterizes and draws the points
    * @param graphics the Graphics object
    */
  override def paint(graphics : Graphics): Unit = {
    super.paint(graphics)
    displayObject()

    val normalized_drawn_points = points.map(point => {
      if(Math.abs(point.head) <= CANVAS_WIDTH / 2 || Math.abs(point(1)) <= CANVAS_HEIGHT / 2) {
        Seq((point.head + CANVAS_WIDTH * 0.5) / CANVAS_WIDTH, (point(1) + CANVAS_HEIGHT * 0.5) / CANVAS_HEIGHT)
      } else {
        println("WARNING : the point (" + point.head + " ; " + point(1) + ") can't be drawn in this canvas.")
        null
      }
    })

    val rasterized_points = normalized_drawn_points.map(point => {
      if(point != null) {
        Seq(point.head * IMAGE_WIDTH, -(1 - point(1) * IMAGE_HEIGHT))
      } else {
        null
      }
    })

    /*val more_points_0 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points.head, rasterized_points(1)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_2 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points.head, rasterized_points(2)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_5 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points.head, rasterized_points(4)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_3 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(1), rasterized_points(3)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_4 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(1), rasterized_points(5)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_6 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(2), rasterized_points(6)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_7 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(6), rasterized_points(7)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_8 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(7), rasterized_points(3)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_9 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(4), rasterized_points(6)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_10 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(5), rasterized_points(7)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_11 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(4), rasterized_points(5)), interpolator.linear).map(_.map(_.intValue()))
    val more_points_12 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.05, Seq(rasterized_points(3), rasterized_points(2)), interpolator.linear).map(_.map(_.intValue()))
    */

    val face_1 : Seq[Seq[Int]] = interpolator.computeAllPossibleLinearlyInterpolatedPointsBetween4PointsFast(Seq(rasterized_points.head, rasterized_points(1), rasterized_points(2), rasterized_points(3)), 0.01, 0.01, interpolator.linear).map(_.map(_.intValue()))
    val face_2 : Seq[Seq[Int]] = interpolator.computeAllPossibleLinearlyInterpolatedPointsBetween4PointsFast(Seq(rasterized_points.head, rasterized_points(1), rasterized_points(4), rasterized_points(5)), 0.01, 0.01, interpolator.linear).map(_.map(_.intValue()))
    val face_3 : Seq[Seq[Int]] = interpolator.computeAllPossibleLinearlyInterpolatedPointsBetween4PointsFast(Seq(rasterized_points(4), rasterized_points(5), rasterized_points(6), rasterized_points(7)), 0.01, 0.01, interpolator.linear).map(_.map(_.intValue()))
    val face_4 : Seq[Seq[Int]] = interpolator.computeAllPossibleLinearlyInterpolatedPointsBetween4PointsFast(Seq(rasterized_points(6), rasterized_points(7), rasterized_points(2), rasterized_points(3)), 0.01, 0.01, interpolator.linear).map(_.map(_.intValue()))
    val face_5 : Seq[Seq[Int]] = interpolator.computeAllPossibleLinearlyInterpolatedPointsBetween4PointsFast(Seq(rasterized_points(1), rasterized_points(3), rasterized_points(5), rasterized_points(7)), 0.01, 0.01, interpolator.linear).map(_.map(_.intValue()))
    val face_6 : Seq[Seq[Int]] = interpolator.computeAllPossibleLinearlyInterpolatedPointsBetween4PointsFast(Seq(rasterized_points.head, rasterized_points(2), rasterized_points(4), rasterized_points(6)), 0.01, 0.01, interpolator.linear).map(_.map(_.intValue()))


    // val more_points = more_points_0.union(more_points_2).union(more_points_3).union(more_points_4).union(more_points_5).union(more_points_6).union(more_points_7).union(more_points_8).union(more_points_9).union(more_points_10).union(more_points_11).union(more_points_12)


    //val more_points_0 : Seq[Seq[Int]] = interpolator.computeAllPossibleInterpolatedPoints(0.1, Seq(rasterized_points.head, rasterized_points(1), rasterized_points(2), rasterized_points(3)), interpolator.cosine).map(_.map(_.intValue()))
    //val more_points = more_points_0


    /*rasterized_points = rasterized_points.map(point => {
      if(point != null) {
        Seq(point.head.toInt, point(1).toInt)
      } else {
        null
      }
    })*/

    // val extended_rasterized_points = rasterized_points.union(more_points)

    val(
      y_max_face_1,
      y_min_face_1,
      ) = getYMaxAndYMinForFace(face_1)
    val(
      x_max_face_1,
      x_min_face_1,
      ) = getXMaxAndXMinForFace(face_1)
    val (noise_map, max_noise_value) = noise_maker.frequencyNoise(y_max_face_1 - y_min_face_1, x_max_face_1 - x_min_face_1)
    face_1.foreach(rasterized_point => {

      graphics.setColor(noise_maker.getColor(
        ((rasterized_point.head.toDouble / x_max_face_1) * (x_max_face_1 - x_min_face_1)).toInt,
        ((rasterized_point(1).toDouble / y_max_face_1) * (y_max_face_1 - y_min_face_1)).toInt,
        noise_map, max_noise_value))

      graphics.fillRect(rasterized_point.head, rasterized_point(1), 5, 5)

      /*graphics.drawString(  // Drawing a legend
        "P(" + rasterized_point.head + " ; " + -(1 - rasterized_point(1)) + ")",
        rasterized_point.head - 50,
        rasterized_point(1) - 10
      )*/

    })

    val(
      y_max_face_2,
      y_min_face_2,
      ) = getYMaxAndYMinForFace(face_2)
    val(
      x_max_face_2,
      x_min_face_2,
      ) = getXMaxAndXMinForFace(face_2)
    val (noise_map_2, max_noise_value_2) = noise_maker.frequencyNoise(y_max_face_2 - y_min_face_2, x_max_face_2 - x_min_face_2)
    face_2.foreach(rasterized_point => {

      graphics.setColor(noise_maker.getColor(
        ((rasterized_point.head.toDouble / x_max_face_2) * (x_max_face_2 - x_min_face_2)).toInt,
        ((rasterized_point(1).toDouble / y_max_face_2) * (y_max_face_2 - y_min_face_2)).toInt,
        noise_map_2, max_noise_value_2))

      graphics.fillRect(rasterized_point.head, rasterized_point(1), 5, 5)

      /*graphics.drawString(  // Drawing a legend
        "P(" + rasterized_point.head + " ; " + -(1 - rasterized_point(1)) + ")",
        rasterized_point.head - 50,
        rasterized_point(1) - 10
      )*/

    })

    val(
      y_max_face_3,
      y_min_face_3,
      ) = getYMaxAndYMinForFace(face_3)
    val(
      x_max_face_3,
      x_min_face_3,
      ) = getXMaxAndXMinForFace(face_3)
    val (noise_map_3, max_noise_value_3) = noise_maker.frequencyNoise(y_max_face_3 - y_min_face_3, x_max_face_3 - x_min_face_3)
    face_3.foreach(rasterized_point => {

      graphics.setColor(noise_maker.getColor(
        ((rasterized_point.head.toDouble / x_max_face_3) * (x_max_face_3 - x_min_face_3)).toInt,
        ((rasterized_point(1).toDouble / y_max_face_3) * (y_max_face_3 - y_min_face_3)).toInt,
        noise_map_3, max_noise_value_3))

      graphics.fillRect(rasterized_point.head, rasterized_point(1), 5, 5)

      /*graphics.drawString(  // Drawing a legend
        "P(" + rasterized_point.head + " ; " + -(1 - rasterized_point(1)) + ")",
        rasterized_point.head - 50,
        rasterized_point(1) - 10
      )*/

    })

    val(
      y_max_face_4,
      y_min_face_4,
      ) = getYMaxAndYMinForFace(face_4)
    val(
      x_max_face_4,
      x_min_face_4,
      ) = getXMaxAndXMinForFace(face_4)
    val (noise_map_4, max_noise_value_4) = noise_maker.frequencyNoise(y_max_face_4 - y_min_face_4, x_max_face_4 - x_min_face_4)
    face_4.foreach(rasterized_point => {

      graphics.setColor(noise_maker.getColor(
        ((rasterized_point.head.toDouble / x_max_face_4) * (x_max_face_4 - x_min_face_4)).toInt,
        ((rasterized_point(1).toDouble / y_max_face_4) * (y_max_face_4 - y_min_face_4)).toInt,
        noise_map_4, max_noise_value_4))

      graphics.fillRect(rasterized_point.head, rasterized_point(1), 5, 5)

      /*graphics.drawString(  // Drawing a legend
        "P(" + rasterized_point.head + " ; " + -(1 - rasterized_point(1)) + ")",
        rasterized_point.head - 50,
        rasterized_point(1) - 10
      )*/

    })

    val(
      y_max_face_5,
      y_min_face_5,
      ) = getYMaxAndYMinForFace(face_6)
    val(
      x_max_face_5,
      x_min_face_5,
      ) = getXMaxAndXMinForFace(face_6)
    val (noise_map_5, max_noise_value_5) = noise_maker.frequencyNoise(y_max_face_5 - y_min_face_5, x_max_face_5 - x_min_face_5)
    face_5.foreach(rasterized_point => {

      graphics.setColor(noise_maker.getColor(
        ((rasterized_point.head.toDouble / x_max_face_5) * (x_max_face_5 - x_min_face_5)).toInt,
        ((rasterized_point(1).toDouble / y_max_face_5) * (y_max_face_5 - y_min_face_5)).toInt,
        noise_map_5, max_noise_value_5))

      graphics.fillRect(rasterized_point.head, rasterized_point(1), 5, 5)

      /*graphics.drawString(  // Drawing a legend
        "P(" + rasterized_point.head + " ; " + -(1 - rasterized_point(1)) + ")",
        rasterized_point.head - 50,
        rasterized_point(1) - 10
      )*/

    })

    val(
      y_max_face_6,
      y_min_face_6,
      ) = getYMaxAndYMinForFace(face_6)
    val(
      x_max_face_6,
      x_min_face_6,
      ) = getXMaxAndXMinForFace(face_6)
    val (noise_map_6, max_noise_value_6) = noise_maker.frequencyNoise(y_max_face_6 - y_min_face_6, x_max_face_6 - x_min_face_6)
    face_6.foreach(rasterized_point => {

      graphics.setColor(noise_maker.getColor(
        ((rasterized_point.head.toDouble / x_max_face_6) * (x_max_face_6 - x_min_face_6)).toInt,
        ((rasterized_point(1).toDouble / y_max_face_6) * (y_max_face_6 - y_min_face_6)).toInt,
        noise_map_6, max_noise_value_6))

      graphics.fillRect(rasterized_point.head, rasterized_point(1), 5, 5)

      /*graphics.drawString(  // Drawing a legend
        "P(" + rasterized_point.head + " ; " + -(1 - rasterized_point(1)) + ")",
        rasterized_point.head - 50,
        rasterized_point(1) - 10
      )*/

    })

    /*extended_rasterized_points.foreach(rasterized_point => {

      graphics.setColor(noise_maker.getColor(rasterized_point.head, rasterized_point(1), noise_map, max_noise_value))
      graphics.fillRect(rasterized_point.head, rasterized_point(1), 5, 5)
      /*graphics.drawString(  // Drawing a legend
        "P(" + rasterized_point.head + " ; " + -(1 - rasterized_point(1)) + ")",
        rasterized_point.head - 50,
        rasterized_point(1) - 10
      )*/

    })*/

  }

}
