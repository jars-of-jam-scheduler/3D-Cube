/**
  * The projector
  */
class Projector {

  /**
    * Computes the coordinates of the projection of the point P on the image view.
    * The image viewis assumed to be 1 unit forward the camera.
    * The computation uses the definition of the similar triangles.
    *
    * @param points the point P we want to project on the image view. Its coordinates must be expressed in the coordinates
    *          system of the camera before using this function.
    * @return the point P', projection of P.
    */
  def projectPointsOnImageView(points : Seq[Seq[Double]]) : Seq[Seq[Double]] = {
    points.map(point => {
      point.map(coordinate => {
        coordinate / -point(2)
      })
    })
  }

}